package ch.bfh.voget5.medical_prescription.server.medreg;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.eclipse.scout.rt.shared.services.lookup.LookupRow;

import ch.bfh.voget5.medical_prescription.shared.doctor.IDoctorLookupService;
import ch.bfh.voget5.medical_prescription.shared.medreg.Doctor;

public class DoctorLookupService implements IDoctorLookupService {

	@Override
	public List<? extends ILookupRow<String>> getDataByText(ILookupCall<String> call) {
		List<ILookupRow<String>> list = new ArrayList<>();
		Set<Doctor> doctorSet = BEANS.get(MedregService.class).getDoctorSet();
		String filterText = call.getText().replace("*", "");
		for (Doctor doctor : doctorSet) {
			if (StringUtility.containsStringIgnoreCase(doctor.getName(), filterText)
					|| StringUtility.containsStringIgnoreCase(doctor.getGln(), filterText)) {
				LookupRow<String> lookupRow = new LookupRow<>(doctor.getGln(), doctor.getName());
				lookupRow.withTooltipText(doctor.getGln());
				list.add(lookupRow);
			}
		}
		return list;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByAll(ILookupCall<String> call) {
		List<ILookupRow<String>> list = new ArrayList<>();
		Set<Doctor> doctorSet = BEANS.get(MedregService.class).getDoctorSet();
		for (Doctor doctor : doctorSet) {
			LookupRow<String> lookupRow = new LookupRow<>(doctor.getGln(), doctor.getName());
			lookupRow.withTooltipText(doctor.getGln());
			list.add(lookupRow);
		}
		return list;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByRec(ILookupCall<String> call) {
		return CollectionUtility.emptyArrayList();
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByKey(ILookupCall<String> call) {
		return CollectionUtility.emptyArrayList();
	}

}
