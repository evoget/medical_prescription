package ch.bfh.voget5.medical_prescription.server.medreg;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.bfh.voget5.medical_prescription.server.blockchain.BlockchainService;
import ch.bfh.voget5.medical_prescription.shared.medreg.Doctor;
import ch.bfh.voget5.medical_prescription.shared.medreg.IMedregService;
import voget5.medical_prescription.shared.webservice.MedregTablePageData;
import voget5.medical_prescription.shared.webservice.MedregTablePageData.MedregTableRowData;

@ApplicationScoped
public class MedregService implements IMedregService {
	static Logger log = LoggerFactory.getLogger(MedregService.class);
	private static final Set<Doctor> doctorSet = initialLoad();

	@Override
	public Set<Doctor> getDoctorSet() {
		return doctorSet;
	}

	private static Set<Doctor> initialLoad() {
		Set<Doctor> newDoctorStore = new HashSet<>();

		newDoctorStore.add(new Doctor("Dr. Volker Katz", "5872154231325"));
		newDoctorStore.add(new Doctor("Dr. Lukas Behrendt", "9287345099732"));
		newDoctorStore.add(new Doctor("Dr. Malte Kehl", "2345234567744"));
		newDoctorStore.add(new Doctor("Dr. Frank Haas", "5123435432455"));
		newDoctorStore.add(new Doctor("Dr. Gabriele Drechsler", "5435454334543"));
		newDoctorStore.add(new Doctor("Dr. Annabell Rössler", "3858334544258"));
		newDoctorStore.add(new Doctor("Dr. Giulia Schwinn", "3478534558866"));
		newDoctorStore.add(new Doctor("Dr. Tara Binder", "4937232943739"));
		newDoctorStore.add(new Doctor("Dr. Leonie Imhof", "1048328203484"));
		return newDoctorStore;
	}

	@Override
	public boolean checkApproval(String gln) {
		for (Doctor doctor : doctorSet) {
			if (StringUtility.equalsIgnoreCase(doctor.getGln(), gln) && doctor.hasApproval()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setApproval(String gln, boolean approval) {
		Set<Doctor> modifiedDoctorSet = new HashSet<>(doctorSet.size());
		for (Doctor doctor : doctorSet) {
			if (StringUtility.equalsIgnoreCase(doctor.getGln(), gln)) {
				doctor.setApproval(approval);
			}
			modifiedDoctorSet.add(doctor);
		}
		doctorSet.clear();
		doctorSet.addAll(modifiedDoctorSet);

		if (approval) {
			BEANS.get(BlockchainService.class).addDoctor(gln);
		} else {
			BEANS.get(BlockchainService.class).removeDoctor(gln);
		}

	}

	@Override
	public MedregTablePageData getMedregTableData(SearchFilter filter) {
		MedregTablePageData pageData = new MedregTablePageData();
		for (Doctor doctor : doctorSet) {
			MedregTableRowData row = pageData.addRow();
			row.setName(doctor.getName());
			row.setGln(doctor.getGln());
			row.setApproval(doctor.hasApproval());
		}
		return pageData;
	}
}
