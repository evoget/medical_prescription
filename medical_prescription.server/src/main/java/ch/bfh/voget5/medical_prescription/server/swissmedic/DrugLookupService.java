package ch.bfh.voget5.medical_prescription.server.swissmedic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.services.lookup.AbstractLookupService;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.eclipse.scout.rt.shared.services.lookup.LookupRow;

import ch.bfh.voget5.medical_prescription.shared.swissmedic.Drug;
import ch.bfh.voget5.medical_prescription.shared.swissmedic.IDrugLookupService;

public class DrugLookupService extends AbstractLookupService<String> implements IDrugLookupService {

	@Override
	public List<? extends ILookupRow<String>> getDataByKey(ILookupCall<String> call) {
		return CollectionUtility.emptyArrayList();
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByRec(ILookupCall<String> call) {
		return CollectionUtility.emptyArrayList();
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByText(ILookupCall<String> call) {
		List<ILookupRow<String>> list = new ArrayList<>();
		Set<Drug> drugSet = BEANS.get(SwissmedicService.class).getDrugSet();
		String filterText = call.getText().replace("*", "");
		for (Drug drug : drugSet) {
			if (StringUtility.containsStringIgnoreCase(drug.getDescription(), filterText)
					|| StringUtility.containsStringIgnoreCase(drug.getApprovalNumber(), filterText)
					|| StringUtility.containsStringIgnoreCase(drug.getActiveIngredient(), filterText)) {
				LookupRow<String> lookupRow = new LookupRow<>(drug.getApprovalNumber(), drug.getDescription());
				lookupRow.withTooltipText(
						drug.getApprovalNumber() + " | " + drug.getCategory() + " | " + drug.getActiveIngredient());
				list.add(lookupRow);
			}
		}
		return list;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByAll(ILookupCall<String> call) {
		List<ILookupRow<String>> list = new ArrayList<>();
		Set<Drug> drugSet = BEANS.get(SwissmedicService.class).getDrugSet();
		for (Drug drug : drugSet) {
			LookupRow<String> lookupRow = new LookupRow<>(drug.getApprovalNumber(), drug.getDescription());
			lookupRow.withTooltipText(
					drug.getApprovalNumber() + " | " + drug.getCategory() + " | " + drug.getActiveIngredient());
			list.add(lookupRow);

		}
		return list;
	}
}
