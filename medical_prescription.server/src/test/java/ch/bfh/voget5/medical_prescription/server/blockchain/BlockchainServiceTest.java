package ch.bfh.voget5.medical_prescription.server.blockchain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.testing.platform.runner.RunWithSubject;
import org.eclipse.scout.rt.testing.server.runner.RunWithServerSession;
import org.eclipse.scout.rt.testing.server.runner.ServerTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.bfh.voget5.medical_prescription.server.ServerSession;
import ch.bfh.voget5.medical_prescription.server.medreg.MedregService;
import ch.bfh.voget5.medical_prescription.server.swissmedic.SwissmedicService;

@RunWithSubject("anonymous")
@RunWith(ServerTestRunner.class)
@RunWithServerSession(ServerSession.class)
public class BlockchainServiceTest {
	private String doctor = UUID.randomUUID().toString();
	private String drug = UUID.randomUUID().toString();
	private String prescriptionHash;

	private BlockchainService blockchainService = BEANS.get(BlockchainService.class);
	private SwissmedicService swissmedicService = BEANS.get(SwissmedicService.class);
	private MedregService medregService = BEANS.get(MedregService.class);

	private Date yesterday = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
	private Date tomorrow = new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000);

	@Before
	public void prepare() {
		medregService.setApproval(doctor, true);
		swissmedicService.setApprovalEndDate(drug, tomorrow);
	}

	@Test
	public void testStorePrescription() {
		prescriptionHash = UUID.randomUUID().toString();
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
	}

	@Test
	public void testRedeemPrescription() {
		prescriptionHash = UUID.randomUUID().toString();
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		assertTrue(blockchainService.redeemPrescription(prescriptionHash, doctor, drug).getResult());
	}

	@Test
	public void testInvalidHashPrescription() {
		prescriptionHash = UUID.randomUUID().toString();
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		assertFalse(blockchainService.redeemPrescription(UUID.randomUUID().toString(), doctor, drug).getResult());
	}

	@Test
	public void testStoreInvalidDoctor() {
		prescriptionHash = UUID.randomUUID().toString();
		medregService.setApproval(doctor, false);
		assertFalse(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		medregService.setApproval(doctor, true);
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
	}

	@Test
	public void testRedeemInvalidDoctor() {
		prescriptionHash = UUID.randomUUID().toString();
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		medregService.setApproval(doctor, false);
		assertFalse(blockchainService.redeemPrescription(prescriptionHash, doctor, drug).getResult());
		medregService.setApproval(doctor, true);
	}

	@Test
	public void testStoreInvalidDrug() {
		prescriptionHash = UUID.randomUUID().toString();
		swissmedicService.setApprovalEndDate(drug, yesterday);
		assertFalse(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		swissmedicService.setApprovalEndDate(drug, tomorrow);
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
	}

	@Test
	public void testRedeemInvalidDrug() {
		prescriptionHash = UUID.randomUUID().toString();
		assertTrue(blockchainService.storePrescription(prescriptionHash, doctor, drug).getResult());
		swissmedicService.setApprovalEndDate(drug, yesterday);
		assertFalse(blockchainService.redeemPrescription(prescriptionHash, doctor, drug).getResult());
		swissmedicService.setApprovalEndDate(drug, tomorrow);
	}
}
