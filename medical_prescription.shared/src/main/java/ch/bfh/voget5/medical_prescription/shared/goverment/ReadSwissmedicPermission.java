package ch.bfh.voget5.medical_prescription.shared.goverment;

import java.security.BasicPermission;

public class ReadSwissmedicPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadSwissmedicPermission() {
		super(ReadSwissmedicPermission.class.getSimpleName());
	}
}
