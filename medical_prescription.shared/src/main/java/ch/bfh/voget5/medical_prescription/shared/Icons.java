package ch.bfh.voget5.medical_prescription.shared;

import org.eclipse.scout.rt.shared.AbstractIcons;

/**
 * @author voget5
 */
public class Icons extends AbstractIcons {

	private static final long serialVersionUID = 1L;

	public static final String SCOUT = "eclipse_scout";
	public static final String USER = "user";
	public static final String APPLOGO = "application_logo";
}
