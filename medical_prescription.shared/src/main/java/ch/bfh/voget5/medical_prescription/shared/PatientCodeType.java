package ch.bfh.voget5.medical_prescription.shared;

import java.util.UUID;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class PatientCodeType extends AbstractCodeType<String, String> {

	private static final long serialVersionUID = 1L;
	public static final String ID = UUID.randomUUID().toString();

	@Override
	public String getId() {
		return ID;
	}

	@Order(1000)
	public static class LenaMeierCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "7563577075405";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("LenaMeier");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	@Order(2000)
	public static class MarkusFischerCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "7563577075406";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("MarkusFischer");
		}

		@Override
		public String getId() {
			return ID;
		}
	}
}
