package ch.bfh.voget5.medical_prescription.shared.blockchain;

import java.io.Serializable;

public class PrescriptionStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	String reason;
	boolean result;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean getResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
}
