package ch.bfh.voget5.medical_prescription.shared.swissmedic;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class DrugCodeType extends AbstractCodeType<Long, String> {

	private static final long serialVersionUID = 1L;
	public static final long ID = 0L;

	@Override
	public Long getId() {
		return ID;
	}

	@Order(1000)
	public static class TestDrugCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "987465432";

		@Override
		protected String getConfiguredText() {
			return "Aspirin";
		}

		@Override
		public String getId() {
			return ID;
		}
	}

}
