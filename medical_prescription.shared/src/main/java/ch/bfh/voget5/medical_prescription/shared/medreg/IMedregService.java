package ch.bfh.voget5.medical_prescription.shared.medreg;

import java.util.Set;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

import voget5.medical_prescription.shared.webservice.MedregTablePageData;

@TunnelToServer
public interface IMedregService extends IService {

	boolean checkApproval(String gln);

	void setApproval(String gln, boolean approval);

	Set<Doctor> getDoctorSet();

	MedregTablePageData getMedregTableData(SearchFilter filter);
}
