package ch.bfh.voget5.medical_prescription.shared.medreg;

public class Doctor {
	private String name;
	private String gln; // Global Location Number / Eindeutige Nummer einer Medizinalperson
	private boolean hasApproval;

	public Doctor(String name, String gln) {
		super();
		this.name = name;
		this.gln = gln;
		this.hasApproval = true;
	}

	public boolean hasApproval() {
		return hasApproval;
	}

	public void setApproval(boolean hasApproval) {
		this.hasApproval = hasApproval;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGln() {
		return gln;
	}

	public void setGln(String gln) {
		this.gln = gln;
	}

}
