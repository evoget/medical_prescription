package ch.bfh.voget5.medical_prescription.shared.doctor;

import java.security.BasicPermission;

public class UpdateDoctorPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateDoctorPermission() {
		super(UpdateDoctorPermission.class.getSimpleName());
	}
}
