package ch.bfh.voget5.medical_prescription.shared.doctor;

import java.security.BasicPermission;

public class ReadDoctorPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadDoctorPermission() {
		super(ReadDoctorPermission.class.getSimpleName());
	}
}
