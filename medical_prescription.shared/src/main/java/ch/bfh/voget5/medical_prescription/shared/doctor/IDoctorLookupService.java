package ch.bfh.voget5.medical_prescription.shared.doctor;

import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.lookup.ILookupService;

@TunnelToServer
public interface IDoctorLookupService extends ILookupService<String> {
}
