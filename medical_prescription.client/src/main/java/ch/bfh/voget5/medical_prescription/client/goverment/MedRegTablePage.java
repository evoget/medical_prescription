package ch.bfh.voget5.medical_prescription.client.goverment;

import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

import ch.bfh.voget5.medical_prescription.client.goverment.MedRegTablePage.Table;
import ch.bfh.voget5.medical_prescription.shared.medreg.IMedregService;
import voget5.medical_prescription.shared.webservice.MedregTablePageData;

@Data(MedregTablePageData.class)
public class MedRegTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Medizinalberuferegister");
	}

	@Override
	protected void execLoadData(SearchFilter filter) {
		importPageData(BEANS.get(IMedregService.class).getMedregTableData(filter));
	}

	public class Table extends AbstractTable {

		public GlnColumn getGlnColumn() {
			return getColumnSet().getColumnByClass(GlnColumn.class);
		}

		public ApprovalColumn getApprovalColumn() {
			return getColumnSet().getColumnByClass(ApprovalColumn.class);
		}

		public NameColumn getNameColumn() {
			return getColumnSet().getColumnByClass(NameColumn.class);
		}

		@Order(1000)
		public class NameColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredWidth() {
				return 250;
			}
		}

		@Order(2000)
		public class GlnColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("gln");
			}

			@Override
			protected int getConfiguredWidth() {
				return 150;
			}
		}

		@Order(3000)
		public class ApprovalColumn extends AbstractBooleanColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approval");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(1000)
		public class EnableMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("enableDoctor");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection, TableMenuType.MultiSelection);
			}

			@Override
			protected void execAction() {
				for (ITableRow row : getSelectedRows()) {
					BEANS.get(IMedregService.class).setApproval(String.valueOf(row.getCellValue(1)), true);
				}
				reloadPage();
			}
		}

		@Order(2000)
		public class DisableMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("disableDoctor");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection, TableMenuType.MultiSelection);
			}

			@Override
			protected void execAction() {
				for (ITableRow row : getSelectedRows()) {
					BEANS.get(IMedregService.class).setApproval(String.valueOf(row.getCellValue(1)), false);
				}
				reloadPage();
			}
		}

	}
}
