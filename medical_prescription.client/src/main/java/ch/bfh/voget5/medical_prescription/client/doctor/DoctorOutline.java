package ch.bfh.voget5.medical_prescription.client.doctor;

import java.util.List;

import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.platform.text.TEXTS;

import ch.bfh.voget5.medical_prescription.shared.Icons;

public class DoctorOutline extends AbstractOutline {
	@Override
	protected void execCreateChildPages(List<IPage<?>> pageList) {
		super.execCreateChildPages(pageList);
		pageList.add(new DoctorPage());
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Doctor");
	}

	@Override
	protected String getConfiguredIconId() {
		return Icons.Pencil;
	}
}
