package ch.bfh.voget5.medical_prescription.client.pharmacy;

import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithNodes;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.platform.text.TEXTS;


public class PharmacyPage extends AbstractPageWithNodes {
	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected boolean getConfiguredTableVisible() {
		return false;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Pharmacy");
	}

	@Override
	protected Class<? extends IForm> getConfiguredDetailForm() {
		return PharmacyForm.class;
	}
}
